# Light Service & Data Connector

> Peter H. Salus in A Quarter-Century of Unix (1994):
> 
> - Write programs that do one thing and do it well.
> - Write programs to work together.
> - Write programs to handle text streams, because that is a universal interface.

**Note**: _[Feature requests and feedback](https://gitlab.com/gaia-x/lab/lsd-connector/-/issues/new) are most welcome!_

## What it is

<!-- Congrats !! you opened the raw file ;)
TL;DR: LSD is to EDC, what k3s is to k8s.
-->

- A connector with built-in compatibility with the Gaia-X trust framework for cloud services and data products.
- The simplest implementation of the [IDSA Dataspace Protocol](https://docs.internationaldataspaces.org/ids-knowledgebase/v/dataspace-protocol).
- A source-code first effort to drive interoperablity with other dataspace connectors implementing the IDSA Dataspace protocol:
    - [FIWARE TRUE Connector](https://github.com/Engineering-Research-and-Development/fiware-true-connector)
    - [Prometheus-X Dataspace Connector](https://github.com/Prometheus-X-association/dataspace-connector.git)
    - other connectors based on the [Eclipse Dataspace Components](https://github.com/eclipse-edc/Connector) framework.

**Note**: _This connector is agnostic from Gaia-X Compliance and intent to be compatible with several trust framework like [iShare](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222125/Admission), [DOME](https://dome-marketplace.eu/about/), ..._

## What it's not

- an identity provider
- a wallet or agent
- a catalogue or marketplace
- the retrofit of the IDSA Dataspace protocol into an existing data exchange software

**Note**: _This connector is not a mandatory component of the [Gaia-X Clearing House](https://gitlab.com/gaia-x/lab/gxdch)._

## Is that yet another dataspace connector ?

No.

The intent of this connector is to extend its use to services (cloud, AI, infra, ...) using the same `IDSA Contract Negotiation protocol` used for data products.

The issues:

- heterogeneity of the service's terms & conditions which prevents the creation of an end-to-end processing pipeline cross service providers.
- existing regulated domains use paper-based certificates.

The proposals:

- link and anchor the negotiated data policies in the services storing, processing or transfering data.
- create a chain of relevant proofs about the enforcement of the those policies on the services (include infrastructure service and infrastructure exchange + data processing services – VM, LB, …)
- support policiy negotiation with certificate’s digital twins based on the [Eclipse Conformity Assessment Policy and Credential Profile](https://projects.eclipse.org/projects/technology.dataspace-cap), to help with the identification of the liabilities.

## Main features

- Discoverability of other connectors on the same network via [Multicast DNS (mDNS)](https://en.wikipedia.org/wiki/Multicast_DNS).
- Callback support with [ngrok](https://ngrok.com/)
- All logging backend supported by Python3: stdout, syslog, HTTP, graylog, ...
- DCAT and ODRL support with [rdflib](https://rdflib.readthedocs.io/en/stable/) and [pySHACL](https://github.com/RDFLib/pySHACL)
- [ODRL with VC](https://www.w3.org/community/odrl/wiki/ODRL_Profiles) profile support (_To be confirmed with OIDC support_)
- Policy engine using [MOSAICrOWN](https://github.com/mosaicrown/policy-engine) (_To be confirmed_)
- Support [RFC9309](https://www.rfc-editor.org/rfc/rfc9309.html) for robots exclusion
- Data transfer using [rsync](https://rsync.samba.org/)

### Whislist

- Support for [ckan](https://github.com/ckan/ckan)
- Support for [OPC UA](https://opcfoundation.org/about/opc-technologies/opc-ua/)
